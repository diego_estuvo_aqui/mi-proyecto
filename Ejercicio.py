# Completa el ejercicio aquí
class Pan:
    masa = "sin definir"
    precio = 0.5
    def __init__(self,masa):
        self.masa = masa

class Bocadillo(Pan):
    masa = "sin definir"
    relleno = "sin definir"
    precio = 1.2
    def __init__(self,masa,relleno):
        super().__init__(masa)
        self.relleno = relleno
        
class Bocadillo_Especial(Bocadillo):
    masa = "sin definir"
    relleno = "sin definir"
    add = "sin definir"
    precio = 1.5
    def __init__(self,masa,relleno,add):
        super().__init__(masa,relleno)
        self.add = add
class Bebidas:
    sabor = "sin definir"
    precio = 1
    def __init__(self,sabor):
        self.sabor = sabor
class Promocion:
    def descuento_2x1_simple(bocadillo):
        descuento = False
        if type(bocadillo).__name__ == "Bocadillo":
            mensaje="Se ha aplicado el descuento de 2x1 en Bocadillos"
            descuento = True
        else: 
            mensaje="No se puede aplicar el descuento a este producto"
        return mensaje,descuento
class Compra:
    productos = []
    def pedido(productos):
        i = 0
        j = 0
        cant_pan = 0
        cant_b = 0
        cant_b_esp = 0
        cant_beb = 0
        descuento = False
        total = 0
        total_Pan = 0
        total_Bocadillo = 0
        total_Bocadillo_Especial = 0
        total_Bebida = 0
        prt_pan = False
        prt_bo = False
        prt_bo_e = False
        prt_beb = False
        for k,i in enumerate(productos):
            if type(i).__name__ == "Pan":
                total += i.precio
                total_Pan += i.precio
                cant_pan += 1
            elif type(i).__name__ == "Bocadillo":
                total += i.precio
                total_Bocadillo += i.precio
                cant_b += 1
            elif type(i).__name__ == "Bocadillo_Especial":
                total += i.precio
                total_Bocadillo_Especial += i.precio
                cant_b_esp += 1
            elif type(i).__name__ == "Bebidas":
                total += i.precio
                total_Bebida += i.precio
                cant_beb += 1
            else:
                print("Error con el producto:",i)
        
        print("Producto \t\t\t Precio \t\t Cant. \t\t\t Total")
        for l,j in enumerate(productos):
            if type(j).__name__ == "Pan" and prt_pan != True :
                prt_pan=True
                print ("{}\t\t\t\t{}\t\t\t{}\t\t\t{}".format("Pan",j.precio,cant_pan,total_Pan))
            elif type(j).__name__ == "Bocadillo" and prt_bo != True:
                prt_bo=True
                print ("{}\t\t\t{}\t\t\t{}\t\t\t{}".format("Bocadillo",j.precio,cant_b,total_Bocadillo))
                if (cant_b>=2):
                    descuento_mensaje_bocadillo,descuento = Promocion.descuento_2x1_simple(j)
                    total -= j.precio
                    desc_aplicado = j.precio
            elif type(j).__name__ == "Bocadillo_Especial" and prt_bo_e != True:
                prt_bo_e=True
                print ("{}\t\t{}\t\t\t{}\t\t\t{}".format("Bocadillo_Especial",j.precio,cant_b_esp,total_Bocadillo_Especial))
            elif type(j).__name__ == "Bebidas" and prt_beb != True:
                prt_beb=True
                print ("{}\t\t\t\t{}\t\t\t{}\t\t\t{}".format("Bebidas",j.precio,cant_beb,total_Bebida))
        if descuento == True:
            print ("===PROMOCIÓN===")
            print ("{}\t\t\t-{}".format(descuento_mensaje_bocadillo,desc_aplicado))
        print("\t\t\t\t\t\t\t\t\t{}{}".format("TOTAL:",total))
    
def comprar_pan(productos,seleccion):
    unid=int(input("¿Cuántas unidades de",type(producto).__name__,"desea?"))
    if(seleccion == "1"):
        productos.append(Pan("Trigo"))
    elif seleccion == "2":
        productos.append(Pan("Integral"))
    elif seleccion == "3":
        productos.append(Pan("Sin gluten"))
    return productos

def comprar_pan(productos,seleccion):
    unid=int(input("¿Cuántas unidades de",type(producto).__name__,"desea?"))
    if(seleccion == "1"):
        productos.append(Pan("Trigo"))
    elif seleccion == "2":
        productos.append(Pan("Integral"))
    elif seleccion == "3":
        productos.append(Pan("Sin gluten"))
    return productos

pan_1 = Pan("Integral")
bocadillo_1 = Bocadillo("Trigo","Jamon")
pan_2 = Pan("Sin gluten")
bocadillo_2 = Bocadillo("Trigo","Queso")
bocadillo_especial_1 = Bocadillo_Especial("Integral","Tortilla","Queso")
Bebida_1 = Bebidas("Café")
Bebida_2 = Bebidas("Coca-cola")
lista = [pan_1,bocadillo_1,pan_2,bocadillo_2,bocadillo_especial_1,Bebida_1,Bebida_2]

compra_1 = Compra.pedido(lista)
