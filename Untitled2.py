#!/usr/bin/env python
# coding: utf-8

# In[ ]:


class Cuadrado:
    def __init__(self,lado):
        self.__lado = lado
        
    def calculo_superficie(self):
        self.__area = self.__lado ** 2
        return self.__area
    
    def SetLado(self,lado):
        self.__lado=lado
        self.__area=self.calculo_superficie() ## ->>>>Al mismo tiempo que se use el Setter para lado, se calcula el área. 
        
    def GetLado(self):
        return self.__lado
    
    def GetArea(self):
        if self.__area is None:
            self.__area = self.calculo_superficie()
        return self.__area

lado = int(input("Introducir medida del lado: "))
cuadrado_ejemplo = Cuadrado(lado)
print("Lado:",cuadrado_ejemplo.GetLado(),"\nArea:",cuadrado_ejemplo.calculo_superficie()) ##-->>>>Usando GetLado y llamando a función calculo superficie

cuadrado_ejemplo.SetLado(10)  ##Uso el Setter para cambiar el atributo Lado

"""-->>>Hago el print del lado y Área usando Getter en ambos casos y comprobando en Área si existe el dato o no. haré más de un print para comprobar el getArearea"""
print("Lado:",cuadrado_ejemplo.GetLado(),"\nArea:",cuadrado_ejemplo.GetArea())
print("Area sin hacer el cálculo porque ya existe el área calculada: ",cuadrado_ejemplo.GetArea())

