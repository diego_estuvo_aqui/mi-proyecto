#!/usr/bin/env python
# coding: utf-8

# In[ ]:


########   DEFINICION DE FUNCIONES    ########
##############################################


def compra(num_billetes, puestos_totales):
    if(puestos_totales < num_billetes):
        mensaje = "ERROR: Sólo hay {} billetes disponibles y haz solicitado {}\n\n".format(puestos_totales,num_billetes)
        resta_billetes_totales = puestos_totales
    else:
        resta_billetes_totales =  puestos_totales - num_billetes
        mensaje = "La operación se ha ejecutado correctamente!\n\t\t\tBiletes comprados: {}\n\n".format(num_billetes)
    return resta_billetes_totales, mensaje

def devolucion(num_billetes, puestos_disponibles, puestos_totales):
    suma_billetes_totales = num_billetes + puestos_disponibles
    if (suma_billetes_totales > puestos_totales):
        mensaje = "ERROR: El límite de puestos del bus es {}\n\t\t\thay {} puestos disponibles y usted desea devolver {}\n\t\t\tpor favor introduzca un valor válido\n\n".format(puestos_totales,puestos_disponibles,num_billetes)
        suma_billetes_totales = puestos_disponibles
    else:
        mensaje = "La operación se ha ejecutado correctamente!\n\t\t\tBiletes devueltos: {}\n\n".format(num_billetes)
    return suma_billetes_totales, mensaje

def estado_bus(puestos_disponibles, puestos_totales):
    puestos_ocupados = puestos_totales - puestos_disponibles
    mensaje = "Puestos totales: {}\n\t\t\tPuestos disponibles: {}\n\t\t\tPuestos ocupados: {}".format(puestos_totales,puestos_disponibles,puestos_ocupados)
    return mensaje

def menu(puestos_totales):
    ejecutar = True
    puestos_disponibles = puestos_totales
    while ejecutar:
        print("Seleccione una opción:")
        print("1) Comprar billetes")
        print("2) Devolver billetes")
        print("3) Estado de compra")
        print("\n\nQ) salir")
        opcion = input("Seleccione opción:")
        if (opcion == "0"):
            ejecutar = False
            print("Salida del menú")
        elif (opcion == "1"):
            print("\t\t\t"+"\033[0;30;42m"+"===Venta de Billetes==="+'\033[0;m')
            billetes = int(input("=¿Cuántos billetes desea?=: ")) 
            puestos_disponibles, texto = compra(billetes,puestos_disponibles)
            print("\t\t\t"+texto,"\n=====================")
        elif (opcion == "2"):
            print("\t\t\t"+"\033[0;30;45m"+"===Devolución de Billetes==="+'\033[0;m')
            billetes = int(input("=¿Cuántos billetes desea devolver?=: ")) 
            puestos_disponibles, texto = devolucion(billetes,puestos_disponibles,puestos_totales)
            print("\t\t\t"+texto,"\n=====================")
        elif (opcion == "3"):
            print("\t\t\t"+"\033[0;30;46m"+"===Estado de la Venta==="+'\033[0;m')
            texto = estado_bus(puestos_disponibles,puestos_totales)
            print("\t\t\t"+texto,"\n=====================")
        elif (opcion == "Q"):
            ejecutar = False
            print("De vuelta a la selección de buses")
        else:
            print("ERROR: Carácter no válido, seleccione una opción válida")
####################################################
#########   INICIO DEL PROGRAMA (MAIN)    ##########
####################################################
program = True           
while program:
    print("\033[0;30;41m"+"Seleccione un bus: "+'\033[0;m')
    print("1) Bus pequeño: 10 puestos")
    print("2) Bus mediano: 15 puestos")
    print("3) Bus grande: 20 puestos")
    print("4) King Kong: 50 puestos")
    print("5) Godzilla: 100 puestos")
    print("\n\n0) Salir")
    program_opcion = input("Seleccionar un bus: ")
    if(program_opcion == "1"):
        menu(10)
    elif(program_opcion == "2"):
        menu(15)
    elif(program_opcion == "3"):
        menu(20)
    elif(program_opcion == "4"):
        menu(50)
    elif(program_opcion == "5"):
        menu(100)
    elif(program_opcion == "0"):
        program = False
        print("¡Gracias por usar el programa, hasta pronto!")
    else: 
        print("Error al introducir input, introduzca un valor válido")
print("Primer Commit")
print("Segundo commit")
print("Tecer commit!!!!")
